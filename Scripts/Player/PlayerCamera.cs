using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float mov = 0.1f;
    private Touch touch;

    void Start()
    {        
        Invoke("StepByStep", 1f);        
    }

    public void StepByStep()
    {   
        transform.Translate(0,0,mov);
        Invoke("StepByStep", 0.01f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x + touch.deltaPosition.x*(mov/2),-16f,16f),
                    transform.position.y,
                    transform.position.z
                );
            }
        }         

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            transform.Translate(-mov,0,0);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            transform.Translate(mov,0,0);
        }
       
    }
}
