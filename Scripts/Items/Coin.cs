using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Coin : MonoBehaviour
{
    public TMP_Text txtCoins;
    protected AudioSource audiosource;

    void Start() 
    {
        audiosource = GetComponent<AudioSource>();
        ShowCoins();
    }

    public void ShowCoins()
    {
        txtCoins.text = "Monedas: "+Score.coins.ToString();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("collider");
        if (other.CompareTag("Player"))
        {
            Debug.Log("collide with player");
            audiosource.Play();
            Score.coins++;
            ShowCoins();
            PutAway(40,45);
            if (Score.coins>2) {
                SceneManager.LoadScene(1);
            }             
        }
    }

    protected void PutAway(int minZ=250, int maxZ=350)
    {
        gameObject.transform.position = new Vector3(
            UnityEngine.Random.Range(-16,16),
            gameObject.transform.position.y,
            UnityEngine.Random.Range(gameObject.transform.position.z+minZ, gameObject.transform.position.z+maxZ)
        );        
    }
}
